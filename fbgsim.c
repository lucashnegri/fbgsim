#include "fbgsim.h"
#include <math.h>
#include <complex.h>
#include <stdlib.h>

/**
 * J or transfer-matrix. Private.
 */
typedef struct {
    double complex j11, j12, j21, j22;
} FBGJMatrix;

const double FBG_NM = 1;
const double FBG_CM = 1e7;

const double pi = 3.14159265358979323846;

FBG* fbg_new(int num_segments) {
    if(num_segments < 1) return NULL;
    
    FBG* fbg = malloc(sizeof(FBG));
    if(!fbg) return NULL;
    
    fbg->segments = malloc( num_segments*sizeof(FBGSegment) );
    if(!fbg->segments) {
        free(fbg);
        return NULL;
    }
    
    fbg->num_segments = num_segments;
    return fbg;
}

void fbg_free(FBG* fbg) {
    free(fbg->segments);
    free(fbg);
}

double fbg_length(FBG* fbg) {
    double len = 0;
    
    for(int i = 0; i < fbg->num_segments; ++i)
        len += fbg->segments[i].length;
    
    return len;
}

double fbg_ideal_length(double desired, double Lambda, int num_segments) {
    const double aux  = Lambda*num_segments;
    const int seg_mul = floor(desired/aux + 0.5);
    return aux*seg_mul;
}

FBG* fbg_uniform(double length, double Lambda, double n_av, double delta_n0,
    int num_segments) {
    FBG* fbg = fbg_new(num_segments);
    if(!fbg) return NULL;
        
    for(int i = 0; i < num_segments; ++i) {
        FBGSegment* seg = fbg->segments+i;
        seg->length     = length / num_segments;
        seg->Lambda     = Lambda;
        seg->n_av       = n_av;
        seg->delta_n0   = delta_n0;
    }
    
    return fbg;
}

void fbg_apod_gaussian(FBG* fbg, double a, double y) {
    const int num_segments  = fbg->num_segments;
    const double length     = fbg_length(fbg);
    double cur_length = 0;
    
    for(int i = 0; i < num_segments; ++i) {
        FBGSegment* seg   = fbg->segments+i;
        const double z    = cur_length + seg->length/2.0;
        cur_length += seg->length;
        
        const double aux = seg->delta_n0*exp(-a*pow( (z-length/2.0)/length, 2) );
        seg->n_av       += y*aux;
        seg->delta_n0    = aux;
    }
}

void fbg_linear_chirp(FBG* fbg, double delta_Lambda) {
    /* 'fbg' must have segments with the same length and Lambda before calling this function */
    const double seg_length = fbg->segments->length;

    /* how much to increment the Lambda at each segment */
    const double delta_per_seg = delta_Lambda * seg_length;
    
    for(int i = 0; i < fbg->num_segments; ++i) {
        FBGSegment* seg  = fbg->segments+i;
        seg->Lambda += (i + 0.5) * delta_per_seg;                   /* middle point */
        seg->length = fbg_ideal_length(seg_length, seg->Lambda, 1); /* guarantee the phase match */
    }
}

FBG* fbg_gaussian(double length, double Lambda, double n_av, double delta_n0,
    int num_segments, double a, double y) {
    
    FBG* fbg = fbg_uniform(length, Lambda, n_av, delta_n0, num_segments);
    fbg_apod_gaussian(fbg, a, y);
    return fbg;
}
 
FBGJMatrix fbg_jmatrix(FBGSegment* seg, double lambda) {
    const double Beta       = 2*seg->n_av*pi/lambda;
    const double Beta0      = pi/seg->Lambda;
    const double delta_Beta = Beta - Beta0;
    const double k          = pi*seg->delta_n0/lambda;
    const double complex s  = csqrt( pow(cabs(k),2) - pow(delta_Beta,2));
    const double l          = seg->length;
    
    FBGJMatrix J;
    J.j11 = ( delta_Beta*csinh(s*l) + I*s*ccosh(s*l) ) / (I*s);
    J.j12 = k*csinh(s*l) / (I*s);
    J.j21 = conj(J.j12);
    J.j22 = conj(J.j11);
    
    return J;
}

double fbg_sim(FBG* fbg, double lambda) {
    double complex a = 0.0, b = 1.0;
    
    for(int i = fbg->num_segments-1; i >= 0; --i) {
        FBGSegment* seg = &fbg->segments[i];
        FBGJMatrix J    = fbg_jmatrix(seg, lambda);
        
        const double complex an = J.j11*a + J.j12*b;
        const double complex bn = J.j21*a + J.j22*b;
        a = an;
        b = bn;
    }
    
    return pow(cabs(a/b), 2);
}

void fbg_sim_range(FBG* fbg, double* out, int points, double from, double to) {
    const double step = (to - from) / (points-1);

    for(int i = 0; i < points; ++i) {
        const double lambda = from + i*step;
        out[i] = fbg_sim(fbg, lambda);
    }
}

void fbg_strain(FBG* fbg, double prof[], double p11, double p12, double vf) {
    for(int i = 0; i < fbg->num_segments; ++i) {
        FBGSegment* seg = fbg->segments+i;
        const double strain   = prof[i] * 1e-3;
        double n03      = seg->n_av;
        n03 = n03*n03*n03;
        
        seg->length *= 1.0 + strain;
        seg->Lambda *= 1.0 + strain;
        seg->n_av   -= 0.5*n03*(p12 - vf*(p11 + p12))*strain;
    }
}
