CC=gcc
OPT=-O3 -march=native -Wall -Wextra -pedantic -fPIC -ffast-math -std=c99
DESTDIR=
PREFIX=/usr/local

.PHONY: all
all: libfbgsim.so tests

libfbgsim.so: fbgsim.c fbgsim.h
	$(CC) -shared $(OPT) fbgsim.c -o libfbgsim.so -lm

.PHONY: tests
tests: libfbgsim.so tests/uniform.c tests/gaussian.c tests/chirped.c 
	$(CC) -Wall -g $(OPT) tests/uniform.c  -o test_uniform  -L. -lfbgsim -Wl,--rpath -Wl,.
	$(CC) -Wall -g $(OPT) tests/gaussian.c -o test_gaussian -L. -lfbgsim -Wl,--rpath -Wl,.
	$(CC) -Wall -g $(OPT) tests/chirped.c  -o test_chirped  -L. -lfbgsim -Wl,--rpath -Wl,.

.PHONY: clean
clean:
	rm -f libfbgsim.so test_uniform test_gaussian test_chirped 
	rm -f o o0 o1 o2 o3

.PHONY: install
install: libfbgsim.so
	cp libfbgsim.so $(DESTDIR)$(PREFIX)/lib/
	cp fbgsim.h     $(DESTDIR)$(PREFIX)/include/
