// simulates an uniform delta_n0 with chirped Lambda FBG
#include <stdio.h>
#include "../fbgsim.h"

int main() {
    const double Lambda       = 517;
    const double delta_Lambda = 5e-7; /* 5nm / cm */
    const double n_av         = 1.5;
    const double delta_n0     = 7e-5;
    const int segments        = 20;
    const double length       = fbg_ideal_length(0.5*FBG_CM, Lambda, segments);
    
    FBG* fbg = fbg_uniform(length, Lambda, n_av, delta_n0, segments);
    fbg_linear_chirp(fbg, delta_Lambda);
    
    /* the modified Lambda and length parameters of each segment */
    for(int i = 0; i < 20; ++i) {
        fprintf(stderr, "%g %g\n", fbg->segments[i].length, fbg->segments[i].Lambda);
    }

    /* compute the new length */
    const double new_length = fbg_length(fbg);
    fprintf(stderr, "Original length: %g, new length: %g\n", length, new_length);
    
    /* simulation */
    for(double lambda = 1550; lambda < 1560; lambda += 0.001) {
        double r = fbg_sim(fbg, lambda);
        printf("%.9f %.9f\n", lambda, r);
    }
    
    fbg_free(fbg);
    
    return 0;
}
