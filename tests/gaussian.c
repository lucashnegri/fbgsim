// simulates a gaussian apodized (delta_n0) FBG
#include <stdio.h>
#include "../fbgsim.h"

int main() {
    double Lambda   = 517;
    double n_av     = 1.5;
    double delta_n0 = 2e-4;
    int segments    = 20;
    double alpha    = 10;
    double gamma    =  0;
    double length = fbg_ideal_length(1*FBG_CM, Lambda, segments);
    
    FBG* fbg = fbg_gaussian(length, Lambda, n_av, delta_n0, segments, alpha, gamma);
    
    for(double lambda = 1550; lambda < 1552.5; lambda += 0.001) {
        printf("%.9f %.9f\n", lambda, fbg_sim(fbg, lambda));
    }
    
    fbg_free(fbg);
    
    return 0;
}
