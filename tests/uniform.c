// simulates an uniform delta_n0/Lambda FBG with lambda_bragg = 1551nm with
// a length of a little more than 1cm (will round to a multiple of Lambda*N)
#include <stdio.h>
#include "../fbgsim.h"

int main() {
    /* parameters */
    double Lambda   = 517; // nm
    double segments = 1;
    double n_av     = 1.5;
    double delta_n0 = 7e-5;
    
    /* creation */
    double length   = fbg_ideal_length(1*FBG_CM, Lambda, segments);
    FBG* fbg = fbg_uniform(length, Lambda, n_av, delta_n0, segments);
    
    /* reflection spectrum simulation */
    for(double lambda = 1550; lambda < 1552; lambda += 0.001) {
        printf("%.9f %.9f\n", lambda, fbg_sim(fbg, lambda));
    }
    
    fbg_free(fbg);
    
    return 0;
}
