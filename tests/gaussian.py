#! /usr/bin/env python3
import sys
sys.path.append('..')

import fbgsim
import numpy as np
import matplotlib.pyplot as plt

fbg = fbgsim.FBG(length=1*fbgsim.CM, Lambda=517, n_av=1.5,
                 delta_n0=1e-4, num_segments=20,
                 alpha=10, gamma=1.0)

wl = np.linspace(1550.0, 1552.5, 1000)
ref = fbg.sim_range(1550.0, 1552.5, 1000)

plt.plot(wl, ref)
plt.show()
