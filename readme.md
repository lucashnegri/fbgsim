# fbgsim

## About

fbgsim is an implementation of the transfer-matrix method shown by [Huang et al][1] to
simulate fiber Bragg gratings. It directly supports uniform and gaussian
apodizations, with or without pitch chirp. It also makes possible for one to
use custom parameters.

## Dependencies

fbgsim is written in C99 and has no external dependencies.

## Install

  $ make
  # make install PREFIX=/usr/local

## Documentation

Documentation is currently unavailable. See tests.
Implementation details are commented on the header file.

## Limitations

fbgsim does not accounts for the (1) self-chirping effect or for (2) wavelength-dependant n_eff.

## License

LGPLv3+.

## Source code and Contact

Repository on [bitbucket][2].
Contact me at lucashnegri@gmail.com.

[1]: http://dx.doi.org/10.1364/AO.34.005003
[2]: https://bitbucket.org/lucashnegri/fbgsim
