#ifndef FBG_SIM_H
#define FBG_SIM_H

/*
 * See:
 * 
 * S. Huang, M. LeBlanc, M. Ohn, and R. Measures, "Bragg intragrating
 * structural sensing", Appl. Opt.  34, 5003-5009 (1995).
 */

/*
 * Structures and definitions
 */

/**
 * Parameters of an FBG segment.
 */
typedef struct {
    double length;   // [nm]
    double Lambda;   // [nm] 
    double n_av;     // [dimensionless]
    double delta_n0; // [dimensionless]
} FBGSegment;

/**
 * An FBG can be modeled as 'one segment' (uniform case) or as
 * a sequence of segments for nonuniform cases.
 */
typedef struct {
    FBGSegment* segments;
    int num_segments;
} FBG;

/* all length values are in  nanometers by default */
extern const double FBG_NM;

/* 5*FBG_CM corresponds to 5cm or 5x10^7nm */
extern const double FBG_CM;

/*
 * Functions
 */

FBG* fbg_new(int num_segments);

/* creates a new, uninitialized FBG */
void fbg_free(FBG* fbg);

FBG* fbg_uniform(double length, double Lambda, double n_av, double delta_n0,
    int num_segments);
    
FBG* fbg_gaussian(double length, double Lambda, double n_av, double delta_n0,
    int num_segments, double a, double y);

/* performs a linear chirping of each segment. delta_Lambda is dimensionless. Assumes that the
 * grating is uniform, i.e, the Lambda and length of each segment is initially the same. It adjusts
 * the length of each segment to guarantee the phase-matching. */
void fbg_linear_chirp(FBG* fbg, double delta_Lambda);

/* performs a Gaussian apodization on the FBG, modifying it in place */
void fbg_apod_gaussian(FBG* fbg, double a, double y);

/* strain in micro-strain */
void fbg_strain(FBG* fbg, double prof[], double p11, double p12, double vf);

/* simulates the FBG at at given wavelength, returning the reflectance. Internally, the complex reflection is computed */
double fbg_sim(FBG* fbg, double lambda);

/* simulates the FBG at a range of points, storing the reflectance in 'out' */
void fbg_sim_range(FBG* fbg, double* out, int points, double from, double to);

/* computes the total length of the FBG */
double fbg_length(FBG* fbg);

/* length were an integer number of periods is obtained per segment, for phase matching */
double fbg_ideal_length(double desired, double Lambda, int num_segments);

#endif
