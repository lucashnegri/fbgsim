import numpy as np
from collections import namedtuple
from cffi import FFI

# load the definitions from the header and load the library
ffi = FFI()

with open('/usr/include/fbgsim.h') as header:
    defs = ''.join(header.readlines()[2:-2])  # skip header guards

ffi.cdef(defs)
FBGSIM = ffi.dlopen('fbgsim')

# all length values are expressed in nm
CM = 1e7
NM = 1

SEG = namedtuple("SEG", "length Lambda n_av delta_n0")

class FBG:

    """Represents a fiber Bragg grating with arbitrary structure."""

    def __init__(self, length, Lambda, n_av, delta_n0, num_segments,
                 alpha=None, gamma=0.0, ideal_length=True):
        """
        Constructs an FBG.

        Parameters
        ----------
        length : float
            Length of the FBG, in nm
        Lambda : float
            Period of the grating, in nm
        n_av : float
            Average refractive index (usually about 1.5)
        delta_n0 : float
            Amplitude of the induced refractive index change (about 1e-4)
        num_segments : int
            Number of uniform segments to slice the structure (>=1)
        alpha : float, only for Gaussian apodization
            Amplitude of the Gaussian-apodized delta_n0
        gamma : float
            Induced nonlinearity for Gaussian apodization
        ideal_length: bool
            If the ideal length should be computed and used automatically
            to guarantee the phase matching between each segment
        """

        if ideal_length:
            length = FBGSIM.fbg_ideal_length(length, Lambda, num_segments)

        if alpha:
            self.ptr = FBGSIM.fbg_gaussian(length, Lambda, n_av, delta_n0,
                                           num_segments, alpha, gamma)
        else:
            self.ptr = FBGSIM.fbg_uniform(length, Lambda, n_av, delta_n0,
                                          num_segments)

        self.num_segments = num_segments

    def __del__(self):
        try:
            FBGSIM.fbg_free(self.ptr)
        except AttributeError:
            pass

    def apod_gaussian(self, alpha, gamma):
        """
        Performs a gaussian modulation in the core refractive index
        to apodize the resulting reflectance.

        Parameters
        ----------
        alpha : float
            Amplitude of the Gaussian-apodized delta_n0
        gamma : float
            Induced nonlinearity for Gaussian apodization
        """
        FBGSIM.fbg_apod_gaussian(self.ptr, alpha, gamma)

    def params(self):
        """
        Returns a list of SEG (named tuple), each tuple corresponding
        to the parameters of a consecutive segment.
        """
        segs = []
        for i in range(self.ptr.num_segments):
            s = self.ptr.segments[i]
            segs.append(SEG(s.length, s.Lambda, s.n_av, s.delta_n0))

        return segs

    def chirp(self, delta_Lambda):
        """
        Performs a linear chirp (increasing variation in each
        segment length).

        Parameters
        ----------
        delta_Lambda : float
            Amount of chirp (dimensionless)
        """
        FBGSIM.fbg_linear_chirp(self.ptr, delta_Lambda)

    def strain(self, profile, p11, p12, vf):
        """
        Strains the FBG with an arbitrary strain profile.

        Parameters
        ----------
        profile : double[num_segments]
            Strain of each segment, in millistrain
        p11 : float
            Photoelastic constant
        p12: float
            Photoelastic constant
        vf : float
            Poisson's ratio
        """

        # check if the profile is a double array with correct size
        assert profile.shape[0] == self.num_segments
        assert profile.dtype == np.double
        profile = np.ascontiguousarray(profile)

        profile_ptr = ffi.cast('double *', profile.ctypes.data)
        FBGSIM.fbg_strain(self.ptr, profile_ptr, p11, p12, vf)

    def length(self):
        """
        Computes the current length of the FBG.

        Returns
        -------
        float :
            Length of the FBG.
        """
        return FBGSIM.fbg_length(self.ptr)

    def sim(self, lambda_):
        """
        Computes the FBG reflectance at a given wavelength.

        Parameters
        ----------
        lambda_ : float
            Wavelength in nm

        Returns
        -------
        float
            Computed reflectance
        """
        return FBGSIM.fbg_sim(self.ptr, lambda_)

    def sim_range(self, from_, to, n_points):
        """
        Computes the FBG reflectance for a given wavelength range.

        Parameters
        ----------
        from_ : float
            Initial wavelength, in nm (inclusive)
        to : float
            Final wavelength, in nm (inclusive)
        n_points : int
            Number of points

        Returns
        -------
        float[points] :
            Numpy array (internal, reused) with the computed reflectance
        """
        try:
            self.out.resize(n_points, refcheck=False)
        except AttributeError:
            self.out = np.empty((n_points,))

        out_ptr = ffi.cast('double *', self.out.ctypes.data)
        FBGSIM.fbg_sim_range(self.ptr, out_ptr, n_points, from_, to)
        return self.out
