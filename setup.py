﻿from setuptools import setup

setup(
    name='fbgsim',
    version='0.1.0',
    description='Fiber Bragg grating simulation by using the transfer matrix method',
    author='Lucas Hermann Negri',
    author_email='lucashnegri@gmail.com',
    packages=['fbgsim'],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Topic :: Scientific/Engineering'
    ]
)
